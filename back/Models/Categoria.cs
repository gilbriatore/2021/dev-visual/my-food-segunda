namespace MyFood.Models {

  public class Categoria {

    public int Id { get; set; }
    public string Nome { get; set; }
    public string Descricao { get; set; }

  }

}

//dotnet-aspnet-codegenerator controller -name CategoriaController -m Categoria -dc MyFoodContext -api --relativeFolderPath Controllers
