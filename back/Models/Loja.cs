namespace MyFood.Models {

  public class Loja {

    public int Id { get; set; }
    public string Nome { get; set; }

  }

}

//dotnet-aspnet-codegenerator controller -name LojaController -m Loja -dc MyFoodContext -api --relativeFolderPath Controllers
