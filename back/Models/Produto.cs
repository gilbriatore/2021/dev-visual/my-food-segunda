namespace MyFood.Models {
  public class Produto {
    public int Id { get; set;}
    public string Nome { get; set;}
    public string Descricao { get; set;}
    public double Preco { get; set; }
    public double Peso { get; set; }
    public string Foto { get; set; }
    public double Desconto { get; set; }
    public Categoria Categoria {get; set;}

  }
}

//dotnet-aspnet-codegenerator controller -name ProdutoController -m Produto -dc MyFoodContext -api --relativeFolderPath Controllers