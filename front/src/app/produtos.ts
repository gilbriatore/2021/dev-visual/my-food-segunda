export const Produtos = [
  {
    codigo: 1,
    nome: "Acompanhamentos",
    descricao: "Manteiga, nata, mel, requeijão salgado, queijo branco.",
    preco: 27.56,
    peso: 160
  },
  {
    codigo: 2,
    nome: "Bruschetta de salmão e abacate",
    descricao: "Pão preto, salmão defumado, queijo Philadelphia, abacate, tomate, ovos.",
    preco: 52.49,
    peso: 180
  }
]