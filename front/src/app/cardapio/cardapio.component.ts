import { CadastroStatusService } from './../services/cadastro-status.service';
import { Component, OnInit } from '@angular/core';
import { Status } from '../models/status.model';

@Component({
  selector: 'app-cardapio',
  templateUrl: './cardapio.component.html',
  styleUrls: ['./cardapio.component.css']
})
export class CardapioComponent implements OnInit {

  displayedColumns: string[] = ['id', 'nome', 'acoes'];
  dataSource: Status[] = [];
  status: Status = {} as Status;

  constructor(protected cadastro:CadastroStatusService) { }

  ngOnInit(): void {
    this.atualizarTabela();
  }

  atualizarTabela() {
    this.cadastro.listar().subscribe(statusRetorno => {
      this.dataSource = statusRetorno;
    });
  }

  salvarStatus(){
    this.cadastro.salvar(this.status).subscribe(() => {
      this.atualizarTabela();
    });
  }

  excluirStatus(id: number){
    this.cadastro.excluir(id).subscribe(() => {
      this.atualizarTabela();
    });
  }


  buscarStatus(id: number) {
    this.cadastro.buscarPorId(id).subscribe(statusRetorno => {
      this.status = statusRetorno;
    })
  }
}