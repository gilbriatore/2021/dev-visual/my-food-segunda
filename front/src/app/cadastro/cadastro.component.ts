import { Component, OnInit } from '@angular/core';

import { Produtos } from '../produtos';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroComponent implements OnInit {

  //produtos: string[] = ['Maçã', 'Laranja', 'Banana'];

  produtos = Produtos;

  constructor() { }

  ngOnInit(): void {
  }

}
