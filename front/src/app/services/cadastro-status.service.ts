import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Status } from '../models/status.model';

@Injectable({
  providedIn: 'root'
})
export class CadastroStatusService {

  constructor(protected http:HttpClient) { }

  listar() : Observable<Status[]>{
    var url = "https://localhost:5001/api/Status";
    return this.http.get<Status[]>(url);
  }

  salvar(status: Status) : Observable<Status>{
    var url = "https://localhost:5001/api/Status";
    if (status.id > 0) {
      url = url + "/" + status.id;
      return this.http.put<Status>(url, status);
    }
    return this.http.post<Status>(url, status);
  }

  buscarPorId(id: number) : Observable<Status>{
    var url = "https://localhost:5001/api/Status/"  + id;
    return this.http.get<Status>(url);
  }

  excluir(id: number) : Observable<Status>{
    var url = "https://localhost:5001/api/Status/"  + id;
    return this.http.delete<Status>(url);
  }
}
